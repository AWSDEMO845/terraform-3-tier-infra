
#################################################################################
#  Requirement - Create 2 ec2 instances making use of count and for_each block 
#################################################################################

# Create a VPC
resource "aws_vpc" "practice_vpc" {
  cidr_block = var.vpc_cidr
  tags = {
    "Name" = "practice-vpc"
  }
}

#### Creating VPC locals 
locals {
  vpc_id = aws_vpc.practice_vpc.id #vpc_id is the Name-of-the-Local and attribute is resource-name.local-name.id
}

###Creating the public subnet subnet 
resource "aws_subnet" "public_subnets" {
  count                   = length(var.public_subnet_cidr)
  vpc_id                  = local.vpc_id
  cidr_block              = var.public_subnet_cidr[count.index]                      #Only used by count is not used
  availability_zone       = data.aws_availability_zones.available.names[count.index] #This ensures the subnets will be created in different AZs
  map_public_ip_on_launch = true
  tags = {
    Name = "public_subnet-${count.index + 1}"
  }
}

####This creates the private subnets 

resource "aws_subnet" "private_subnets" {
  for_each   = var.private_subnet_cidr
  vpc_id     = local.vpc_id
  cidr_block = each.value.cidr_block #Only used by count is not used
  #availability_zone       = data.aws_availability_zones.available.names[count.index] #Only valid if using for_each with locals 
  availability_zone       = each.value.availability_zone
  map_public_ip_on_launch = true
  tags = {
    Name = each.key
  }
}

###Count solution 

resource "aws_instance" "web" {
  count             = 2
  ami               = data.aws_ami.ami.id #datasource 
  availability_zone = data.aws_availability_zones.available.names[count.index]
  #instance_type     = length(var.instance_type) ##Might not work with count 
  instance_type = var.instance_type[count.index]            ##This could work instead of length
  key_name      = data.aws_key_pair.my_key_pair.key_name    #(data.resource-name.local-name.attribute-name)  #Use data source to pull this 
  subnet_id     = aws_subnet.public_subnets[count.index].id #(use elements)
  #subnet_id         = element([aws_subnet.public_subnets.id,aws_subnet.private_subnets.id], count.index) #Use this if all subnets are count created

  tags = {
    Name = "web-${count.index + 1}"
  }
}

####For each solution 

locals {
  private_instance = {
    instance_1 = {
      ami                 = data.aws_ami.ami.id
      availability_zone   = data.aws_availability_zones.available.names[0]
      instance_type_index = 0
      key_name            = data.aws_key_pair.my_key_pair.key_name
      subnet_id           = aws_subnet.private_subnets["private_subnet_1"].id
    }
    instance_2 = {
      ami                 = data.aws_ami.ami.id
      availability_zone   = data.aws_availability_zones.available.names[1]
      instance_type_index = 1
      key_name            = data.aws_key_pair.my_key_pair.key_name
      subnet_id           = aws_subnet.private_subnets["private_subnet_2"].id
    }
  }

}
resource "aws_instance" "for-each" {
  for_each          = local.private_instance
  ami               = each.value.ami
  instance_type     = var.instance_type[each.value.instance_type_index]
  availability_zone = each.value.availability_zone
  tags = {
    Name = each.key
  }
}