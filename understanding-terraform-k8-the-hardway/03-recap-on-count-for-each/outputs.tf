
#####Output the public Ips of subnets 

output "public_ip_legacy_splat" {
  value = aws_instance.web.*.public_ip
}

output "public_ip" {
  value = aws_instance.web[*].public_ip
}

#This last one will give us everything about instance - ALL METADATA
output "all_metadata" {
  value = aws_instance.web
}