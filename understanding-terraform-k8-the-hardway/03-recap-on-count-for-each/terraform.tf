terraform {
  required_version = ">=1.3" #means anyone with terraform 1.3.0+ can use this code 

  backend "s3" {
    bucket = "terraform-deland-bucket"
    key    = "path/env"       #Any name here is fine - This means statefile will be folder called path in s3 and statfile will be env
    region = "us-east-1"
    dynamodb_table = "terraform-lock"
    encrypt     = true 
  }
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.0" #This is not our constrainst
    }
  }
}