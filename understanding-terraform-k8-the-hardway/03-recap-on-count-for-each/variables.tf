variable "vpc_cidr" {
  type        = string
  description = "This is the cidr of the VPC"
  default     = "10.0.0.0/16"
}

variable "public_subnet_cidr" {
  type        = list(any)
  description = "This is the cidr block of the public subnets"
  default     = ["10.0.0.0/24", "10.0.2.0/24"]
}

variable "private_subnet_cidr" {
  description = "This is the cidr block of the private subnets"
  default = {
    private_subnet_1 = {
      cidr_block        = "10.0.1.0/24"
      availability_zone = "us-east-1a"
    }
    private_subnet_2 = {
      cidr_block        = "10.0.3.0/24"
      availability_zone = "us-east-1b"
    }
  }
}

variable "key_name" {
  type        = string
  description = "name of keypair to pull down using datasource"
  default     = "terraform-eks"
}

variable "instance_type" {
  type        = list(any)
  description = "This is the list of allowed instance types"
  default     = ["t2.micro", "t3.micro", "t2.small"]
}