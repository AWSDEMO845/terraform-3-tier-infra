
####################################################################################
####### CREATING 3 TIER VPC
####################################################################################

resource "aws_vpc" "this" {
  cidr_block           = var.vpc_cidr
  enable_dns_support   = var.enable_dns_support
  enable_dns_hostnames = var.enable_dns_hostnames
  tags = {
    Name    = "3-tier-vpc-${terraform.workspace}"
    env     = "${terraform.workspace}"
    purpose = var.resource-purpose
  }
}

locals {
  vpc_id               = aws_vpc.this.id
  az                   = data.aws_availability_zones.available.names
  public_subnet_ids    = [for subnet in values(aws_subnet.public_subnet) : subnet.id] #Need to learn this 
  private_instance_ids = [for instance in values(aws_instance.private_instance) : instance.id]
  database_subnet_ids  = [for subnet in values(aws_subnet.database_subnet) : subnet.id]
}

####################################################################################
####### CREATING 3 TIER Internet Gateway 
####################################################################################

resource "aws_internet_gateway" "gw" {
  vpc_id = local.vpc_id

  tags = {
    Name    = "3-tier-igw-${terraform.workspace}"
    env     = "${terraform.workspace}"
    purpose = var.resource-purpose
  }
}

####################################################################################
####### CREATING 3 TIER Subnets - Public, Private and Database
####################################################################################

####LOCALS FOR THE PUBLIC SUBNETS 
locals {
  public_subnets = {
    public_subnet-1 = {
      availability_zone = local.az[0]
      cidr_block_index  = 0
    }
    public_subnet-2 = {
      availability_zone = local.az[1]
      cidr_block_index  = 1
    }
  }
}

resource "aws_subnet" "public_subnet" {
  for_each                = local.public_subnets
  vpc_id                  = local.vpc_id
  availability_zone       = each.value.availability_zone
  cidr_block              = var.public_subnet_cidr[each.value.cidr_block_index]
  map_public_ip_on_launch = true

  tags = {
    Name    = "3-tier-${each.key}-${terraform.workspace}"
    env     = "${terraform.workspace}"
    purpose = var.resource-purpose

  }
}

#Locals for the private subnets - Will host EC2 instances 
locals {
  private_subnets = {
    private_subnet-1 = {
      availability_zone = local.az[0]
      cidr_block_index  = 0
    }
    private_subnet-2 = {
      availability_zone = local.az[1]
      cidr_block_index  = 1
    }
  }
}

resource "aws_subnet" "private_subnet" {
  for_each          = local.private_subnets
  vpc_id            = local.vpc_id
  cidr_block        = var.private_subnet_cidr[each.value.cidr_block_index]
  availability_zone = each.value.availability_zone

  tags = {
    Name    = "3-tier-${each.key}-${terraform.workspace}"
    env     = "${terraform.workspace}"
    purpose = var.resource-purpose

  }
}

####LOCALS FOR DATABASE-SUBNET

locals {
  database_subnets = {
    database_subnet-1 = {
      availability_zone = local.az[0]
      cidr_block_index  = 0
    }
    database_subnet-2 = {
      availability_zone = local.az[1]
      cidr_block_index  = 1
    }
  }
}

resource "aws_subnet" "database_subnet" {
  for_each          = local.database_subnets
  vpc_id            = local.vpc_id
  cidr_block        = var.database_subnet_cidr[each.value.cidr_block_index]
  availability_zone = each.value.availability_zone
  tags = {
    Name    = "3-tier-${each.key}-${terraform.workspace}"
    env     = "${terraform.workspace}"
    purpose = var.resource-purpose

  }
}

####################################################################################
#CREATING PUBLIC ROUTE TABLES AND ASSOCIATE WITH PUBLIC SUBNET 
####################################################################################

resource "aws_route_table" "public_route_table" {
  vpc_id = local.vpc_id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.gw.id
  }

  tags = {
    Name    = "3-tier-public-RT-${terraform.workspace}"
    env     = "${terraform.workspace}"
    purpose = var.resource-purpose
  }
}

####################################################################################
#CREATING PUBLIC ROUTE TABLES ASSOCIATIONS
####################################################################################

resource "aws_route_table_association" "a" {
  for_each       = local.public_subnets
  subnet_id      = aws_subnet.public_subnet[each.key].id
  route_table_id = aws_route_table.public_route_table.id
}
#####for count use aws_subnet.public_subnet[keys(aws_subnet.public_subnet)[count.index]].id or aws_subnet.public_subnet.*.id

####################################################################################
#CREATING DEFAULT ROUTE TABLE AND ASSOCIATE WITH PRIVATE SUBNET 
####################################################################################

resource "aws_default_route_table" "this" {
  default_route_table_id = aws_vpc.this.default_route_table_id #(aws_vpc.vpc_logical_name.attriubte)

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_nat_gateway.this.id
  }

  tags = {
    Name    = "3-tier-default-RT-${terraform.workspace}"
    env     = "${terraform.workspace}"
    purpose = var.resource-purpose
  }
}

####################################################################################
#CREATING NAT GATEWAY 
####################################################################################

resource "aws_nat_gateway" "this" {
  depends_on    = [aws_internet_gateway.gw]
  allocation_id = aws_eip.eip.id
  subnet_id     = aws_subnet.public_subnet["public_subnet-1"].id

  tags = {
    Name    = "gw-NAT-${terraform.workspace}"
    env     = "${terraform.workspace}"
    purpose = var.resource-purpose
  }

}

####################################################################################
#CREATING AN ELASTICIP
####################################################################################

resource "aws_eip" "eip" {
  depends_on = [aws_internet_gateway.gw]
  vpc        = true

  tags = {
    Name    = "eip-${terraform.workspace}"
    env     = "${terraform.workspace}"
    purpose = var.resource-purpose
  }
}

