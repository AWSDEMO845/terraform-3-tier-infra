Building a Highly available and scalable 3 tier application on AWS using terraform end-to-end

Procedues 
- Create the architecture diagram 
- Create the s3 bucket and dynamoDB if not - these were created in project 04-statefile (if you haven't created this, do it in the main.tf or a backend.tf file) 
- Setup the backend in terraform.tf
- Create the folder called 06-3-tier and then the files main.tf, output.tf and then reference the backend in terraform.tf 
- Setup the providers.tf and terraform.tf 
- Create the different workspaces using terraform workspace new workspace-name for example in this project 3 workspaces were created - sbx, prod and dev using the commands terraform workspace new sbx, terraform workspace new prod and terraform workspace new dev
- Create the tfvars for each workspace once its workspace is created 
- Now start writing code to create VPC starting with VPC 
- 
''''''
terraform init 
terraform workspace list 
terraform workspace new workspace-name (sbx and dev and prod each at a time)
terraform workspace select sbx or select dev or select prod (depends on account you want to work with)
terraform plan -var-file sbx.file or terraform plan -var-file dev.file etc
terraform apply -var-file sbx.file or terraform plan -var-file dev.file etc

From above you can switch to the workspace, modify command and apply 

''''''

NB: Because we are using workspaces, terraform will create an env folder in s3 bucket and within that bucket, it will have dev, sbx and prod and within them their statefiles 

NB: Also run the command << terraform plan -var-file dev.tfvars -no-color > terraform.plan >>> to export the terraform plan details for boss to see for infra approval 