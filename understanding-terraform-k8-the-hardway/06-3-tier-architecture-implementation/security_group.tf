
####################################################################################
#CREATING BASTION HOST SECURITY GROUP
####################################################################################

resource "aws_security_group" "bastion_sg" {
  name        = "bastion-sg-${terraform.workspace}"
  description = "Allow ssh inbound traffic"
  vpc_id      = local.vpc_id

  ingress {
    description = "Allow ingress traffic from port 22"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["104.230.207.13/32"] #IMPORTANT           #In real life, this is SG of your work VPN 

    #The IP in box above is mine 
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name    = "bastion-sg-${terraform.workspace}"
    env     = "${terraform.workspace}"
    purpose = var.resource-purpose
  }
}

####################################################################################
#CREATING PRIVATE EC2-INSTANCES SECURITY GROUP EGRESS RULE
####################################################################################

resource "aws_security_group" "private_sg" {
  name        = "private-sg-${terraform.workspace}"
  description = "Allow ssh from public sg"
  vpc_id      = local.vpc_id

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name    = "private-sg-${terraform.workspace}"
    env     = "${terraform.workspace}"
    purpose = var.resource-purpose
  }
}

####################################################################################
#CREATING PRIVATE EC2-INSTANCES SECURITY GROUP RULE INGRESS RULE FOR SSH PURPOSES
####################################################################################

resource "aws_security_group_rule" "allow_ssh_from_bastion_sg" {
  security_group_id        = aws_security_group.private_sg.id
  type                     = "ingress"
  from_port                = 22
  to_port                  = 22
  protocol                 = "tcp"
  source_security_group_id = aws_security_group.bastion_sg.id #Reference sg of bastion

}

####################################################################################
#CREATING LOAD BALANCER SECURITY GROUP
####################################################################################

resource "aws_security_group" "alb_sg" {
  name        = "alb-sg-${terraform.workspace}"
  description = "Allow traffic on port 80"
  vpc_id      = local.vpc_id

  ingress {
    description = "Allow HTTP traffic from port 80"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "Allow HTTPS traffic from port 443"
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name    = "alb-sg-${terraform.workspace}"
    env     = "${terraform.workspace}"
    purpose = var.resource-purpose
  }
}

####################################################################################
#CREATING PRIVATE EC2-INSTANCES SECURITY GROUP RULE INGRESS RULE PORT 80 ALB TRAFFIC
####################################################################################

resource "aws_security_group_rule" "allow_http_traffic_from_alb" {
  security_group_id        = aws_security_group.private_sg.id
  type                     = "ingress"
  from_port                = 80
  to_port                  = 80
  protocol                 = "tcp"
  source_security_group_id = aws_security_group.alb_sg.id #Reference sg of ALB_SG
}

####################################################################################
#CREATING REGISTRATION APP INGRESS SECURITY GROUP RULE 
####################################################################################

resource "aws_security_group_rule" "registration_sg_receive_http" {
  security_group_id        = aws_security_group.registration_sg.id
  type                     = "ingress"
  from_port                = 8080
  to_port                  = 8080
  protocol                 = "tcp"
  source_security_group_id = aws_security_group.alb_sg.id #Reference sg of ALB_SG
}

####################################################################################
#CREATING REGISTRATION-APP SECURITY GROUP
####################################################################################

resource "aws_security_group" "registration_sg" {
  name        = "registration_sg-${terraform.workspace}"
  description = "Allow registration-app to receive traffic from ALB sg"
  vpc_id      = local.vpc_id

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name    = "registration_sg-${terraform.workspace}"
    env     = "${terraform.workspace}"
    purpose = var.resource-purpose
  }
}

####################################################################################
#CREATING DATABASE INGRESS SECURITY GROUP RULE 
####################################################################################

resource "aws_security_group_rule" "receive_traffic_from_registration-app" {
  security_group_id        = aws_security_group.database_sg.id
  type                     = "ingress"
  from_port                = 3306
  to_port                  = 3306
  protocol                 = "tcp"
  source_security_group_id = aws_security_group.registration_sg.id #Reference sg of REGISTRATION_SG
}

####################################################################################
#CREATING DATABASE SECURITY GROUP
####################################################################################

resource "aws_security_group" "database_sg" {
  name        = "mysql-sg-${terraform.workspace}"
  description = "Allow database to receive traffic from Registration sg"
  vpc_id      = local.vpc_id

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name    = "mysql-sg-${terraform.workspace}"
    env     = "${terraform.workspace}"
    purpose = var.resource-purpose
  }
}