vpc_cidr = "10.0.0.0/16"

public_subnet_cidr = ["10.0.0.0/24", "10.0.2.0/24"]

private_subnet_cidr = ["10.0.1.0/24", "10.0.3.0/24"]

database_subnet_cidr = ["10.0.5.0/24", "10.0.6.0/24"]

resource-purpose     = "For prod account"
enable_dns_support   = true
enable_dns_hostnames = true

public_instance_type = "t3.small" #Referencing here only because Instance type changes only in prod

private_instance_type = "t3.small"

dns_name                  = "machala-devops.click"
subject_alternative_names = ["*.machala-devops.click"]