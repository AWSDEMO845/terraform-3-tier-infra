####################################################################################
#CREATING PUBLIC EC2-INSTANCES 
####################################################################################

##LOCALS FOR THE PUBLIC INSTANCE FOR_EACH


resource "aws_instance" "public_instance" {
  ami                  = data.aws_ami.ami.id
  instance_type        = var.public_instance_type #use t3.small for prod : t2.micro for sbx and dev
  subnet_id            = aws_subnet.public_subnet["public_subnet-1"].id
  iam_instance_profile = aws_iam_instance_profile.instance_profile.name
  key_name             = var.key_name
  security_groups      = [aws_security_group.bastion_sg.id] #Security_groups ends with s hence accepts list
  lifecycle {
    ignore_changes = [security_groups]
  }
  tags = {
    Name    = "bastion-host-${terraform.workspace}"
    env     = "${terraform.workspace}"
    purpose = var.resource-purpose
  }
}

####################################################################################
#CREATING THE 2 PRIVATE EC2-INSTANCES THAT WILL HOST APP-1 AND APP-2
####################################################################################

##LOCALS FOR THE PUBLIC INSTANCE FOR_EACH OF APP INSTANCES 

locals {
  private_app_instances = {
    app1-instance = {
      availability_zone = local.az[0]
      subnet_id         = aws_subnet.private_subnet["private_subnet-1"].id
      user_data         = file("./templates/app1.sh") #Used to read files within an absolute path
    }
    app2-instance = {
      availability_zone = local.az[1]
      subnet_id         = aws_subnet.private_subnet["private_subnet-2"].id
      user_data         = file("${path.module}/templates/app2.sh") #This is better because .(which means pwd is usually not understood by terraform) and the ${path.module} means a . 
    }
  }
}


resource "aws_instance" "private_instance" {
  for_each             = local.private_app_instances
  ami                  = data.aws_ami.ami.id
  instance_type        = var.private_instance_type #use t3.small for prod : t2.micro for sbx and dev
  subnet_id            = each.value.subnet_id
  iam_instance_profile = aws_iam_instance_profile.instance_profile.name #Instance profile name
  key_name             = var.key_name
  security_groups      = [aws_security_group.private_sg.id] #Security_groups ends with s hence accepts list
  availability_zone    = each.value.availability_zone
  lifecycle {
    ignore_changes = [security_groups]
  }
  tags = {
    Name    = "${each.key}-${terraform.workspace}"
    env     = "${terraform.workspace}"
    purpose = var.resource-purpose
  }
}

####################################################################################
#CREATING THE AUTO-SCALING GROUP THAT WILL LAUNCH REGISTRATION APP  
####################################################################################

resource "aws_launch_template" "registration-app-LT" {
  name                                 = "registration-app-LT-${terraform.workspace}"
  instance_type                        = var.private_instance_type
  image_id                             = data.aws_ami.ami.id
  vpc_security_group_ids               = [aws_security_group.registration_sg.id]
  key_name                             = var.key_name
  instance_initiated_shutdown_behavior = "terminate"
  iam_instance_profile {
    name = aws_iam_instance_profile.instance_profile.name
  }

  # user_data = file("${path.module}/templates/app2.sh")
  user_data = base64encode(templatefile("${path.module}/templates/registration_app.tmpl",
    {
      application_version = "v1.2.01"
      hostname            = "${aws_db_instance.registration_app_db.address}"
      port                = "${var.port}"
      db_name             = "${var.db_name}"
      db_username         = "${var.db_username}"
      db_password         = "${local.encoded_db_password}"
    }
  ))

  tag_specifications {
    resource_type = "instance"

    tags = {
      Name    = "registration-app-instance-${terraform.workspace}"
      env     = "${terraform.workspace}"
      purpose = var.resource-purpose
    }
  }
  monitoring {
    enabled = true
  }
}

resource "aws_autoscaling_group" "registration-app-ASG" {
  depends_on          = [aws_launch_template.registration-app-LT]
  name                = "registration-app-ASG-${terraform.workspace}"
  vpc_zone_identifier = values(aws_subnet.private_subnet)[*].id
  desired_capacity    = 2
  max_size            = 3
  min_size            = 1
  target_group_arns   = [aws_lb_target_group.registration_app-tg.arn] #This is the target group attachments for these instances 

  launch_template {
    id      = aws_launch_template.registration-app-LT.id
    version = "$Latest"
  }
  health_check_type = "EC2"

  initial_lifecycle_hook {
    default_result       = "CONTINUE"
    heartbeat_timeout    = 300 ##This is in seconds 
    lifecycle_transition = "autoscaling:EC2_INSTANCE_LAUNCHING"
    name                 = "ExampleStartUpLifecycleHook"
    notification_metadata = jsonencode(
      {
        hello = "world"
      }
    )
  }

  initial_lifecycle_hook {
    default_result       = "CONTINUE"
    heartbeat_timeout    = 300 ##This is in seconds 
    lifecycle_transition = "autoscaling:EC2_INSTANCE_TERMINATING"
    name                 = "ExampleTerminateLifecycleHook"
    notification_metadata = jsonencode(
      {
        hello = "world"
      }
    )
  }

  tag {
    key = "component"
    value = "${var.component}"
    propagate_at_launch = true
  }

  instance_refresh {                                 #Minimizes downtown
    strategy = "Rolling"  #UPDATE
    triggers = ["tag", "desired_capacity", "max_size"]   ##If any of these change, instance refresh kick in 
  }
  lifecycle {
    create_before_destroy = true 
  }
}

