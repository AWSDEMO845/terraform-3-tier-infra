####################################################################################
#CREATING APPLICATION LB
####################################################################################

resource "aws_lb" "this" {
  name               = "web-alb-${terraform.workspace}"
  internal           = false
  load_balancer_type = "application"
  security_groups    = [aws_security_group.alb_sg.id]
  for_each           = local.public_subnets
  subnets            = local.public_subnet_ids #== [for i in aws_subnet.public_subnet: i.id]

  enable_deletion_protection = false #true for Work or best practice

  tags = {
    Name    = "web-alb-${terraform.workspace}"
    env     = "${terraform.workspace}"
    purpose = var.resource-purpose
  }
}

####################################################################################
#CREATING TARGET GROUPS FOR APP-1 INSTANCE AND APP-2 INSTANCE AND ATTACHMENTS 
####################################################################################

locals {
  lb-target-group = {
    app-1-tg = {
      health_check = {
        path = "/app1/index.html"
      }
    }
    app-2-tg = {
      health_check = {
        path = "/app2/index.html"
      }
    }
  }
}
resource "aws_lb_target_group" "app-1-and-2-tg" {
  for_each    = local.lb-target-group
  name        = "${each.key}-${terraform.workspace}"
  port        = 80
  protocol    = "HTTP"
  vpc_id      = local.vpc_id
  target_type = "instance"
  health_check {
    healthy_threshold   = 3
    unhealthy_threshold = 3
    interval            = 30 #After 30 seconds
    protocol            = "HTTP"
    path                = each.value.health_check.path
    matcher             = "200-399"
    port                = "traffic-port" #Traffic port
  }
}

resource "aws_lb_target_group_attachment" "app-1" {
  target_group_arn = aws_lb_target_group.app-1-and-2-tg["app-1-tg"].arn
  target_id        = aws_instance.private_instance["app1-instance"].id
  port             = 80
}

resource "aws_lb_target_group_attachment" "app-2" {
  target_group_arn = aws_lb_target_group.app-1-and-2-tg["app-2-tg"].arn
  target_id        = aws_instance.private_instance["app2-instance"].id #resource-name.logical-name[for_each_key].id or attribute
  port             = 80
}

####################################################################################
#CREATING TARGET GROUPS FOR REGISTRATION APP AND ATTACHMENTS 
####################################################################################

resource "aws_lb_target_group" "registration_app-tg" {
  name        = "registration-app-tg-${terraform.workspace}"
  port        = 8080
  protocol    = "HTTP"
  vpc_id      = local.vpc_id
  target_type = "instance"
  health_check {
    healthy_threshold   = 3
    unhealthy_threshold = 3
    interval            = 30 #After 30 seconds
    protocol            = "HTTP"
    path                = "/login" #Decided by who created application
    matcher             = "200-399"
    port                = "traffic-port" #Traffic port
  }
}

resource "aws_lb_target_group_attachment" "registration_app" {
  target_group_arn = aws_lb_target_group.app-1-and-2-tg["app-1-tg"].arn
  target_id        = aws_instance.private_instance["app1-instance"].id
  port             = 8080
}


####################################################################################
#CREATING LISTERNER HTTP (REDIRECT) AND HTTPS RULES FOR ALB 
####################################################################################

resource "aws_lb_listener" "http-redirect" {
  for_each          = local.lb-target-group
  load_balancer_arn = aws_lb.this["public_subnet-1"].arn
  port              = 80
  protocol          = "HTTP"

  default_action {
    type = "redirect"

    redirect {
      port        = "443"
      protocol    = "HTTPS"
      status_code = "HTTP_301"
    }
  }
}

##This fixed response will cover all target groups 
resource "aws_lb_listener" "https-forward" {
  load_balancer_arn = aws_lb.this["public_subnet-1"].arn
  port              = 443
  protocol          = "HTTPS"
  ssl_policy        = "ELBSecurityPolicy-2016-08"

  certificate_arn = aws_acm_certificate.this.arn
  default_action {
    type = "fixed-response"

    fixed_response {
      content_type = "text/plain"
      message_body = "Fixed response content"
      status_code  = "200"
    }
  }
}

####################################################################################
#CREATING 3 DIFFERENT LISTERNER RULES FOR THE 3 DIFFERENT APPLICATIONS  
####################################################################################
locals {
  lb_listener_rule = {
    app1 = {
      priority = 1
      action = {
        target_group_arn = aws_lb_target_group.app-1-and-2-tg["app-1-tg"].arn
      }
      condition = {
        path_pattern = {
          values = ["/app1*"]
        }
      }
    }
    app2 = {
      priority = 2
      action = {
        target_group_arn = aws_lb_target_group.app-1-and-2-tg["app-2-tg"].arn
      }
      condition = {
        path_pattern = {
          values = ["/app2*"]
        }
      }
    }
    registration-app = {
      priority = 3
      action = {
        target_group_arn = aws_lb_target_group.registration_app-tg.arn
      }
      condition = {
        path_pattern = {
          values = ["/*"]
        }
      }
    }
  }
}
resource "aws_lb_listener_rule" "app1" {
  for_each     = local.lb_listener_rule
  listener_arn = aws_lb_listener.https-forward.arn ##All coming from https
  priority     = each.value.priority

  action {
    type             = "forward"
    target_group_arn = each.value.action.target_group_arn
  }

  condition {
    path_pattern {
      values = each.value.condition.path_pattern.values
    }
  }
}

####################################################################################
#CREATING ROUTE 53 RECORDS AND AWS CERTIFICATE MANAGER CERRTIFICATE  
####################################################################################
resource "aws_route53_record" "www" {
  zone_id = data.aws_route53_zone.this.zone_id
  name    = var.dns_name
  type    = "A"

  alias {
    name                   = aws_lb.this["public_subnet-1"].dns_name #FYI same as aws_lb.this["public_subnet-2"].dns_name
    zone_id                = aws_lb.this["public_subnet-1"].zone_id  #FYI same as aws_lb.this["public_subnet-2"].dns_name
    evaluate_target_health = true
  }
}

resource "aws_acm_certificate" "this" {
  domain_name               = data.aws_route53_zone.this.name
  subject_alternative_names = var.subject_alternative_names
  validation_method         = "DNS"

  options {
    certificate_transparency_logging_preference = "ENABLED"
  }

  tags = {
    Name    = "acm-cert-${terraform.workspace}"
    env     = "${terraform.workspace}"
    purpose = var.resource-purpose
  }
}

resource "aws_acm_certificate_validation" "this" {
  certificate_arn         = aws_acm_certificate.this.arn
  validation_record_fqdns = [for record in aws_route53_record.example : record.fqdn]
}

resource "aws_route53_record" "example" {
  for_each = {
    for dvo in aws_acm_certificate.this.domain_validation_options : dvo.domain_name => {
      name   = dvo.resource_record_name
      record = dvo.resource_record_value
      type   = dvo.resource_record_type
    }
  }

  allow_overwrite = true
  name            = each.value.name
  records         = [each.value.record]
  ttl             = 60
  type            = each.value.type
  zone_id         = data.aws_route53_zone.this.zone_id
}
