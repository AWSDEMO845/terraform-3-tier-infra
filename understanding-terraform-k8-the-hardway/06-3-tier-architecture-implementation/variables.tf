variable "region" {
  description = "This defines the region we will using"
  type        = string
  default     = "us-east-1"
}

variable "vpc_cidr" {
  description = "This is vpc_cidr"
  type        = string
}

variable "resource-purpose" {
  description = "This is the purpose of the resource"
  type        = string
}

variable "enable_dns_support" {
  type        = bool
  description = "defines the dns support value if it will true/false dependent on account"
}

variable "enable_dns_hostnames" {
  type        = bool
  description = "defines the dns hostnames value if it will true/false dependent on account"
}

variable "key_name" {
  type        = string
  description = "name of keypair to pull down using datasource"
  default     = "terraform-eks"
}

variable "public_subnet_cidr" {
  description = "This is public_subnet_cidr"
  type        = list(any)
}

variable "private_subnet_cidr" {
  description = "This is public_subnet_cidr"
  type        = list(any)
}

variable "database_subnet_cidr" {
  description = "This is public_subnet_cidr"
  type        = list(any)
  #type        = map(any)           #Use this when variabilizing for_each values like in sbx.tfvars of 05-terraform-workspaces
}

variable "public_instance_type" {
  description = "This is the instance type for the public instances"
  type        = string
  default     = "t2.micro"
}

variable "private_instance_type" {
  description = "This is the instance type for the public instances"
  type        = string
  default     = "t2.micro"
}

variable "availability_zones" {
  type = set(string)
}

variable "instance_class" {
  type    = string
  default = "db.t3.micro"
}

variable "db_username" {
  type    = string
  default = "kojitechs"
}

variable "port" {
  type    = number
  default = 3306
}

variable "db_name" {
  type    = string
  default = "webappdb"
}

variable "dns_name" {
  description = "This is the dns name of that would be used to connect to all applications"
  type        = string
}

variable "subject_alternative_names" {
  type = list(any)
}