vpc_cidr = "100.0.0.0/16"

public_subnet_cidr = ["100.0.0.0/24", "100.0.2.0/24"]

private_subnet_cidr = ["100.0.1.0/24", "100.0.3.0/24"]

database_subnet_cidr = ["100.0.5.0/24", "100.0.6.0/24"]

availability_zones = ["us-east-1a", "us-east-1b"]

resource-purpose     = "For sbx account"
enable_dns_support   = false
enable_dns_hostnames = false

dns_name                  = "machala-devops.click"
subject_alternative_names = ["*.machala-devops.click"]