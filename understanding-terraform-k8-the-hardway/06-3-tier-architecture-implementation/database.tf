
###These values will be seen in secret manager once Terraform apply is executed 
locals {
  db_credentials = {
    endpoint    = aws_db_instance.registration_app_db.address
    db_username = var.db_username
    db_name     = var.db_name
    port        = var.port
    password    = random_password.password.result #Read and stored in Secret Manager
  }
}

###These secret manager resources will create secret manager and add db creds in it 
resource "aws_secretsmanager_secret" "example" {
  name        = "mysql-secrets-${terraform.workspace}"
  description = "Secrets to manage mysql superuser password"
}

resource "aws_secretsmanager_secret_version" "example" {
  secret_id     = aws_secretsmanager_secret.example.id
  secret_string = jsonencode(local.db_credentials)
}

resource "random_password" "password" {
  length  = 16
  special = false
}
locals {
  encoded_db_password = base64encode(random_password.password.result)
}

resource "aws_db_subnet_group" "mysql_subnet-group" {
  name       = "registration_subnetgroup-${terraform.workspace}"
  subnet_ids = local.database_subnet_ids

  tags = {
    Name    = "registration_subnetgroup-${terraform.workspace}"
    env     = "${terraform.workspace}"
    purpose = var.resource-purpose
  }

}

resource "aws_db_instance" "registration_app_db" {
  allocated_storage    = 20
  db_name              = "webappdb" #Application team provides this so that app could write to specific schema 
  identifier           = "registration"
  engine               = "mysql"
  engine_version       = "5.7"
  instance_class       = var.instance_class
  username             = var.db_username #"foo"
  port                 = var.port
  password             = random_password.password.result #Will not be stored in any file 
  db_subnet_group_name = aws_db_subnet_group.mysql_subnet-group.name
  skip_final_snapshot  = true #SHOULD BE false IN PROD since we require it there 
  #   final_snapshot_identifier = "this-final"
  vpc_security_group_ids = [aws_security_group.database_sg.id]
}