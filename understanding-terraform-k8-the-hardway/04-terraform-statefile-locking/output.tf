output "bucket_name" {
  value = aws_s3_bucket.this.*.id   #Can also use aws_s3_bucket.this.*.name
}

output "bucket_name_arn" {
  value = aws_s3_bucket.this.*.arn   #Can also use aws_s3_bucket.this.*.name
}