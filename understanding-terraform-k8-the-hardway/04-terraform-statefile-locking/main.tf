resource "aws_s3_bucket" "this" {
  count = length(var.bucket_name)

  bucket = var.bucket_name[count.index]

  #This lifecycle policy ensures that someone cannot destroy this bucket even by mistake
lifecycle {
    prevent_destroy = true    #boolean
}
}


resource "aws_dynamodb_table" "dynamodb-terraform-lock" {
  name           = "terraform-lock"
  hash_key       = "LockID"       #MOST IMPORTANT and this is what terraform reads
  read_capacity  = 20
  write_capacity = 20

  attribute {
    name = "LockID"
    type = "S"
  }
  lifecycle {
    prevent_destroy = true    #This is a policy in terraform 
  }

  tags = {
    Name        = "dynamodb-table-1"
    Environment = "production"
  }
}