variable "bucket_name" {
    type        = list 
    description = "This is the name of the statefile bucket"
    default     = ["terraform-deland-bucket",
                    "05-terraform.workspace-deland",
                    "06-3-tier-architecture-implementation-deland"
                    ]
}