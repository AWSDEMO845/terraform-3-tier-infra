# Sign and name 
{} # curry-braces - every block in terraform starts like this and the things within it
#is called an argument for example cidr_block and tags etc and the attribute here will
#inlcude things like "10.0.0.0/16" etc

() # parathenses 
[] # square brackets 
! # Exclamation sign
& # and sign or amp symbol 
| # or
"" # double string 
'' #single string 
< # less than operator 
> # greater than operator 
% # percentage 
\ # backward slash or backslash 
/ # forward slash 
- # hyphen 
= or == # equal sign 
${} #concatenate 
<= # less than or equal to
>= # greater than or equal to
!= # Not equal 
