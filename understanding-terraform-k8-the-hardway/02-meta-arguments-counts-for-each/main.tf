#####################################
#COUNT FOR META ARGUMENTS
#####################################


# Create a VPC
# resource "aws_vpc" "practice_vpc" {
#   cidr_block             = var.vpc_cidr
#   tags                   = {
#     "Name"               = "practice-vpc"
#   }
# }

#  ##DATA SOURCE 
# resource "aws_subnet" "public_subnets" {
# #  count                   = 4
#   count                   = length(var.public_subnet_cidr) #This is an advance concept that tells terraform to check the length of list in variable and created based off that number 
#   vpc_id                  = local.vpc_id #This is local.Name-of-local
#   cidr_block              = var.public_subnet_cidr[count.index] #This allows for the subnets to pick their IPs based on list in variable.tf
#   #availability_zone      = data.aws_availability_zones.available.names[0] #This ensures subnets are created in 1 az only
#   availability_zone       = data.aws_availability_zones.available.names[count.index] #This ensures the subnets will be created in different AZs
#   map_public_ip_on_launch = true #Ensures that all resources launched in here will get a public IP 
#   tags = {
#     Name                  = "public_subnet-${count.index + 1}"  #This is a string concatination that allows unique tags be given to each subnet"
#   }
#   #In this block of code, there are 4 arguments (vpc_id, cidr_block etc)
# }

# resource "aws_subnet" "private_subnet" {
#   #count                     = 3
#   count                     = length(var.private_subnet_cidr) #This is an advance concept that tells terraform to check the length of list in variable and created based off that number 
#   vpc_id                    = local.vpc_id  ##This is local.Name-of-local
#   cidr_block                = var.private_subnet_cidr[count.index] #This allows for the subnets to pick their IPs based on list in variable.tf
#   #availability_zone        = data.aws_availability_zones.available.names[1] #This ensures subnets are created in 1 az only
#   availability_zone         = data.aws_availability_zones.available.names[count.index] #This ensures the subnets will be created in different AZs
#   tags = {
#     Name                    = "private_subnet-${count.index + 1}"  #This is a string concatination that allows unique tags be given to each subnet
#   }
# }

# resource "aws_instance" "web" {
#   #ami                      = "ami-02f3f602d23f1659d" # us-east-1 use datasource to pull this down
#   ami                       = data.aws_ami.amazon_linux.id
#   instance_type             = "t3.micro"
#   subnet_id                 = aws_subnet.public_subnets[0].id
#   tags                      = {
#     Name                    = "web"
#   }
# }