terraform {
  required_version = ">=1.3" #means anyone with terraform 1.3.0 + can use this code 
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.0" #This is not our constrainst
    }
  }
}