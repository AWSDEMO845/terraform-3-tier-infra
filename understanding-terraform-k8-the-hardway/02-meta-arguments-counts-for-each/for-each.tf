#####################################
#FOR EACH FOR META ARGUMENTS 
#####################################
# Create a VPC
resource "aws_vpc" "for-each_vpc" {
  cidr_block = var.vpc_cidr
  tags = {
    "Name" = "practice-vpc"
  }
}
###########################################
# LOCALS FOR FOR_EACH USE
###########################################

locals {
  public_subnet = {
    pub_subnet_1 = {
      cidr_block        = "10.0.1.0/24"
      availability_zone = "us-east-1a"
    }
    pub_subnet_2 = {
      cidr_block        = "10.0.3.0/24"
      availability_zone = "us-east-1b"
    }
  }
}

locals {
  private_subnet = {
    private_subnet_1 = {
      cidr_block        = "10.0.0.0/24"
      availability_zone = data.aws_availability_zones.available.names[0]
    }
    private_subnet_2 = {
      cidr_block        = "10.0.2.0/24"
      availability_zone = data.aws_availability_zones.available.names[1]
    }
    private_subnet_3 = {
      cidr_block        = "10.0.4.0/24"
      availability_zone = data.aws_availability_zones.available.names[3]
    }
    private_subnet_4 = {
      cidr_block        = "10.0.6.0/24"
      availability_zone = data.aws_availability_zones.available.names[4]
    }
  }
}
##public_subnet is the local name


##DATA SOURCE 
resource "aws_subnet" "public_subnets_for-each" {
  for_each = var.public_subnets-for_each #This is referencing using variable.tf 
  #The following are how to use for_each if using the variables in the variable.tf
  cidr_block        = each.value.cidr_block
  availability_zone = each.value.availability_zone

  vpc_id = local.vpc_id #This is local.Name-of-local

  map_public_ip_on_launch = true #Ensures that all resources launched in here will get a public IP 
  tags = {
    Name = each.key
  }
  #In this block of code, there are 4 arguments (vpc_id, cidr_block etc)
}

#Private subnet is using local reference while public is using variable reference
resource "aws_subnet" "private_subnet_for-each" {
  for_each          = local.private_subnet
  vpc_id            = local.vpc_id ##This is local.Name-of-local
  cidr_block        = each.value.cidr_block
  availability_zone = each.value.availability_zone
  tags = {
    Name = each.key
  }
}