####################################
#   ROOT MODULES
###################################

#Below is a child module 
# module "vpc" {
#   source = "terraform-aws-modules/vpc/aws"

#   name = "module-vpc"
#   cidr = "10.0.0.0/16"

#   # azs             = ["us-east-1a", "us-east-1b", "us-east-1c"]
#   azs             = [data.aws_availability_zones.available.names[0],
#                     data.aws_availability_zones.available.names[1],
#                     data.aws_availability_zones.available.names[2]] #This works because we didnt use count
#   private_subnets = [var.module_private_subnet_cidr[0],var.module_private_subnet_cidr[1],var.module_private_subnet_cidr[2]]  #This works only because we didnt use count in this module
#   public_subnets  = [var.module_public_subnet_cidr[0], var.module_public_subnet_cidr[1],var.module_public_subnet_cidr[2]] #This works only because we didnt use count in this module

#   enable_nat_gateway = true
#   enable_vpn_gateway = true

#   tags = {
#     Terraform = "true"
#     Environment = "dev"
#   }
# }