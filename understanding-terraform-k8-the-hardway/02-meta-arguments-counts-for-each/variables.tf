variable "vpc_cidr" {
  type        = string
  description = "value for VPC CIDR"
  default     = "10.0.0.0/16"
}
variable "subnet_1" {
  type        = string
  description = "value for subnet_1 CIDR"
  default     = "10.0.1.0/24"
}

variable "subnet_2" {
  type        = string
  description = "value for subnet_2 CIDR"
  default     = "10.0.2.0/24"
}

#These subnet variables are used for the count argument
variable "public_subnet_cidr" {
  type        = list(any)
  description = "value for subnet CIDR"
  default     = ["10.0.1.0/24", "10.0.3.0/24", "10.0.5.0/24", "10.0.7.0/24"]
}

variable "private_subnet_cidr" {
  description = "value for subnet CIDR"
  default     = ["10.0.2.0/24", "10.0.4.0/24", "10.0.6.0/24"]
}

#This is the variable for the resources in modules 
variable "module_private_subnet_cidr" {
  description = "This is for the module subnets"
  default     = ["10.0.1.0/24", "10.0.2.0/24", "10.0.3.0/24"]
}

variable "module_public_subnet_cidr" {
  description = "This is for the module subnets"
  default     = ["10.0.101.0/24", "10.0.102.0/24", "10.0.103.0/24"]
}

#These az variables were created for variability of subnet az
variable "subnet_1_az" {
  type        = string
  description = "value for subnet 1 AZ"
  default     = "us-east-1a"
}

variable "subnet_2_az" {
  type        = string
  description = "value for subnet 2 AZ"
  default     = "us-east-1b"
}

#############################################
# VARIABLES FOR FOR_EACH
#############################################

variable "public_subnets-for_each" {
  type        = map(any)
  description = "objects of for_each public_subnets to be created"
  default = {
    pub_subnet_1 = {
      cidr_block        = "10.0.1.0/24"
      availability_zone = "us-east-1a"
    }
    pub_subnet_2 = {
      cidr_block        = "10.0.3.0/24"
      availability_zone = "us-east-1b"
    }
  }
}