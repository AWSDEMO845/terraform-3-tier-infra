####################################
#   VPC Local
###################################

#The have keys and value and when  creating local block, use locals with s 
#When referencing the local, use the word local 

# locals {
#   vpc_id = aws_vpc.practice_vpc.id  #vpc_id is the Name-of-the-Local and attribute is resource-name.local-name.id
# }

locals {
  vpc_id = aws_vpc.for-each_vpc.id #vpc_id is the Name-of-the-Local and attribute is resource-name.local-name.id
}
