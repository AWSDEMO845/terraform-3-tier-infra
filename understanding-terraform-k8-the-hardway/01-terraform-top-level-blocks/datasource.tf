# Declare the data source
data "aws_availability_zones" "available" {
  state = "available"
}


#data.aws_availability_zones.available.name
#["us-east-1a","us-east-1b","us-east-1c","us-east-1d","us-east-1e","us-east-1f"]

data "aws_ami" "amazon_linux" {
  most_recent      = true
  owners           = ["amazon"]

  filter {
    name   = "name"  #This is the AMI Name on the AMI page in console
    values = ["amzn2-ami-kernel-5.10-hvm-*"]  #This is the AMI Name on the AMI page in console
  }

  filter {
    name   = "root-device-type"
    values = ["ebs"]    #This is the Root device type on the AMI page in console
                    #You can get this value by searching using the AMI ID for amazon linux
  }
}

#command to apply single resource terraform apply -target data.aws_ami.amazon_linux