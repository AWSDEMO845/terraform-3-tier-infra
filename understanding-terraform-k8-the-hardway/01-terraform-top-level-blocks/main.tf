# Create a VPC
resource "aws_vpc" "practice_vpc" {
  cidr_block = var.vpc_cidr
  tags = {
    "Name" = "practice-vpc"
  }
}

##DATA SOURCE 
resource "aws_subnet" "subnet_1" {
  vpc_id            = local.vpc_id #This is local.Name-of-local
  cidr_block        = var.subnet_1_cidr
  availability_zone = data.aws_availability_zones.available.names[0]

  tags = {
    Name = "subnet_1"
  }
  #In this block of code, there are 4 arguments (vpc_id, cidr_block etc)
}

resource "aws_subnet" "subnet_2" {
  vpc_id            = local.vpc_id  ##This is local.Name-of-local
  cidr_block        = var.subnet_2_cidr 
  availability_zone = data.aws_availability_zones.available.names[1]

  tags = {
    Name = "subnet_2"
  }
}

resource "aws_instance" "web" {
  #ami          = "ami-02f3f602d23f1659d" # us-east-1 use datasource to pull this down
  ami           = data.aws_ami.amazon_linux.id
  instance_type = "t3.micro"
  #subnet_id     =
  tags          = {
    Name        = "web"
  }
}