variable "vpc_cidr" {
    type        = string 
    description = "value for VPC CIDR"
    default     = "10.0.0.0/16"
}
variable "subnet_1_cidr" {
    type        = string 
    description = "value for subnet CIDR"
    default     = "10.0.1.0/24"
}

variable "subnet_2_cidr" {
    type        = string 
    description = "value for subnet CIDR"
    default     = "10.0.2.0/24"
}

variable "subnet_1_az" {
    type        = string 
    description = "value for subnet 1 AZ"
    default     = "us-east-1a"
}

variable "subnet_2_az" {
    type        = string 
    description = "value for subnet 2 AZ"
    default     = "us-east-1b"
}