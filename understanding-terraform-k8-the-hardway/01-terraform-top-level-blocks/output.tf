####################################
#   GET PUBLIC IP 
###################################
#The attribute here is public_ip which can be gotten from the right side of the ec2
#terraform page and link is https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/instance#attributes-reference

output "public_ip" {
  description = "Fetch the public IP"
  value       = aws_instance.web.public_ip #(resource-name.local-name.attribute-name)
}