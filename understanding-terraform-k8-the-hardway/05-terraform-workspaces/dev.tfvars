vpc_cidr           = "200.0.0.0/16"
public_subnet_cidr = ["200.0.0.0/24", "200.0.2.0/24"]
private_subnet_cidr = {
  private_subnet_1 = {
    cidr_block        = "200.0.1.0/24"
    availability_zone = "us-east-1a"
  }
  private_subnet_2 = {
    cidr_block        = "200.0.3.0/24"
    availability_zone = "us-east-1b"
  }
}
instance_type = ["t2.micro","t3.micro"]
resource-purpose = "For dev use"
