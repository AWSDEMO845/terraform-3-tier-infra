variable "vpc_cidr" {
  type        = string
  description = "This is the cidr of the VPC"
  #Do not pass a default value since we will be using .tfvars and want different values in each account
}

variable "public_subnet_cidr" {
  type        = list(any)
  description = "This is the cidr block of the public subnets"
  #Do not pass a default value since we will be using .tfvars and want different values in each account
}

variable "private_subnet_cidr" {
  description = "This is the cidr block of the private subnets"
}

variable "database_subnet_cidr" {
  description = "This is public_subnet_cidr"
  type        = map(any)
}

variable "key_name" {
  type        = string
  description = "name of keypair to pull down using datasource"
  default     = "terraform-eks"
}

variable "instance_type" {
  type        = list(any)
  description = "This is the list of allowed instance types"

}

variable "resource-purpose" {
    type       = string 
    description = "This defines the purpose of the resource"
}
