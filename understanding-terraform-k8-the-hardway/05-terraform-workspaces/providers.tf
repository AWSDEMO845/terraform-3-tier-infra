provider "aws" {
  region = "us-east-1"

  ##// ONE AWS ACCOUNT (dev,sbx and prod)
  profile = "default" #If profile is default, no need to specify 

  ###This is the approach for multiple SSO account 
  
#   assume_role {
#     role_arn = ""
#   }
}