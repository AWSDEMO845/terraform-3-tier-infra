vpc_cidr           = "100.0.0.0/16"
public_subnet_cidr = ["100.0.0.0/24", "100.0.2.0/24"]
private_subnet_cidr = {
  private_subnet_1 = {
    cidr_block        = "100.0.1.0/24"
    availability_zone = "us-east-1a"
  }
  private_subnet_2 = {
    cidr_block        = "100.0.3.0/24"
    availability_zone = "us-east-1b"
  }
}
database_subnet_cidr = {
  database_subnet-1 = {
    cidr_block        = "100.0.5.0/24"
    availability_zone = "us-east-1a" 
  }
  database_subnet-2 = {
    cidr_block        = "100.0.6.0/24"
    availability_zone = "us-east-1b"    #Cannot be further variabalized 
  }
}
instance_type = ["t2.micro","t3.micro"]
resource-purpose = "For sbx use"
