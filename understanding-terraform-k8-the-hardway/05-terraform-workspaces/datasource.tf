data "aws_ami" "ami" {
  most_recent = true
  owners      = ["amazon"]

  filter {
    name   = "name"
    values = ["amzn2-ami-kernel-5.10-hvm-*"]
  }

  filter {
    name   = "root-device-type"
    values = ["ebs"]
  }
}

#Pulling down the keypair 
data "aws_key_pair" "my_key_pair" {
  key_name           = var.key_name
  include_public_key = true
  filter {
    name   = "tag:Name" #This data is located in the tag area of key 
    values = ["terraform-eks-keypair"]
  }

  #run data.aws_key_pair.my_key_pair under terraform console to see if this datasource works
}

#Pulling the availability zones 
data "aws_availability_zones" "available" {
  state = "available"
}
